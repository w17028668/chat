import firebase from 'firebase/app';
import "firebase/auth";

class AuthController {

    public user: firebase.User;
    public detachListener: Function;

    init() {
        let config = {
            apiKey: "AIzaSyCk75_wpl_tau_cO-lPG47_32D2cYTxVZ4",
            authDomain: "ionic-chat-1fb61.firebaseapp.com",
            databaseURL: "https://ionic-chat-1fb61.firebaseio.com",
            projectId: "ionic-chat-1fb61",
            storageBucket: "ionic-chat-1fb61.appspot.com",
            messagingSenderId: "729074964026",
            appId: "1:729074964026:web:3643161f848f66d1d88703"
        };
        // Initialize Firebase
        firebase.initializeApp(config);
    }

    async waitForAuth(): Promise<firebase.User> {
        return new Promise(resolve => {
            this.detachListener = firebase.auth().onAuthStateChanged(user => {
                if (user) {
                    this.user = user;
                    this.detachListener();
                    resolve(user);
                }
            });
        });
    }

    async getCurrentAuth(): Promise<firebase.User> {
        return new Promise(resolve => {
            this.detachListener = firebase.auth().onAuthStateChanged(user => {
                this.user = user;
                this.detachListener();
                resolve(user);
            });
        });
    }

    async loginAnon(displayName?: string): Promise<firebase.auth.UserCredential> {
        try {
            const userCredential = await firebase.auth().signInAnonymously();

            if (displayName) { await userCredential.user.updateProfile({ displayName: displayName }); }
            return userCredential;
        } catch (error) { console.log(error); }
    }
    async logOut(): Promise<void> { return await firebase.auth().signOut(); }
}

export const AuthService = new AuthController();
