import { AuthService } from "./auth";
import firebase from "firebase/app";
import "firebase/firestore";

class DatabaseController {

    private db: firebase.firestore.Firestore;
    public detachListener: Function;

    init() {
        this.db = firebase.firestore();
    }

    watchMessages(handler: Function): void {
        this.detachListener = this.db
            .collection("messages") 
            .orderBy("created", "desc") 
            .limit(50)
            .onSnapshot(querySnapshot => {
                let messages = [];
                querySnapshot.forEach(doc => {
                    messages.push({
                        id: doc.id,
                        ...doc.data()
                    });
                })
                handler(messages) 
            });
    }

    async addMessage(message: string):  
    Promise<firebase.firestore.DocumentReference> {
        try {
            return await this.db.collection("messages").add({
                uid: AuthService.user.uid,
                created: Date.now(),
                message: message,
                author: AuthService.user.displayName 
            });
        } catch (error) {
            console.log(error);
            
        }
    }
    
}

export const DatabaseService = new DatabaseController();